package demo;

/**
 * 發生例外狀況的範例
 * 
 * @author Tim
 *
 */
public class Practice1Demo {
	public static void main(String[] args) {

		int x = 12;
		int y = 0;

		int result = x / y;
		System.out.println(x + "/" + y + "=" + result);
	}
}

/**投機方法用if else避免發生例外**/
/**正規方法用try catch捕捉例外**/