package demo;

public class Throw2Demo {
	public static void main(String[] args) throws CustomException  {
		int x = 10, y = 0, result;

		if (y == 0) {
			throw new CustomException("除數不能為0");
		} else {
			result = x / y;

			System.out.println("運算結果=" + result);
		}

	}
}
