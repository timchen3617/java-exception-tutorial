package demo;

import java.io.IOException;

public class tryCatch1Demo {
  public static void main(String[] args) {

    int a[] = new int[7];
    int arrayIndex = 0;

    IOExceptionDemo iOExceptionDemo = null;
    try {
      // 指定a陣列索引arrayIndex值
      a[arrayIndex] = 30 / 5;
      // 讀取檔案myFile.txt
      iOExceptionDemo = new IOExceptionDemo();
      iOExceptionDemo.readFile();
    } catch (ArithmeticException e) {
      e.printStackTrace();
      System.out.println("Warning: ArithmeticException " + e.getMessage());

    } catch (ArrayIndexOutOfBoundsException | NullPointerException e) {
      e.printStackTrace();
      System.out.println("Warning: ArrayIndexOutOfBoundsException or NullPointerException "
          + e.getMessage());

    } catch (Exception e) {
      e.printStackTrace();
      System.out.println("Warning: Some Other exception " + e.getMessage());
    } finally {
      System.out.println("不論如何一定會執行的區塊...");
    }
    
    System.out.println("Out of try-catch block...");
    System.out.println("陣列a索引" + arrayIndex + " = " + a[arrayIndex]);
    if (iOExceptionDemo.fileData.length() > 0) {

      System.out.println("讀取檔案成功:\n"+iOExceptionDemo.fileData.toString());
    }else{
      System.out.println("讀取檔案失敗");
    }

  }
}
