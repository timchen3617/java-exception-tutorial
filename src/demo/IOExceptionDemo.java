package demo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class IOExceptionDemo {
  public StringBuilder fileData = new StringBuilder(); 
  
  public void readFile() throws Exception {
    BufferedReader br = null;
    
    try {
      br = new BufferedReader(new FileReader(
              "/Users/Tim/repos/bitbucket/java-exception-tutorial/src/file/myFile.txt"));
      
      String contentLine = "";
      while ((contentLine = br.readLine()) != null) {
        fileData.append(contentLine);
        System.out.println(contentLine);
      }
    } catch (IOException e) {
      //throw new CustomIOException("找不到myFile.txt檔");
      throw e;
    } finally {
      try {
        if (br != null)
          br.close();
      } catch (IOException e) {
        System.out.println("Error in closing the BufferedReader");
      }
    }
  }
}
